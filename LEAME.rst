﻿
Proyecto Victor Paris
======================


Funcionalidades
----------------

Con este proyecto se pretende conocer un poco más de cerca el próximo mundial
de futbol que se celebrará en Brasil el próximo año 2014.

Consta de:

Página inicial
+++++++++++++++

Información
++++++++++++

Contacto
+++++++++


Instrucciones de instalación en un servidor web en Debian 2.6.26
-----------------------------------------------------------------

Pasos para la instalacion
++++++++++++++++++++++++++

Como primer paso debemos acerciorarnos que el sistema este actualizado de lo contrario ejecutamos el comando apt-get update.
Procedemos entonces a instalar el paquete para el servidor Web en debian llamado Apache2,ejecutamos el comando apt-get install apache2.

Pasos para la configuración
++++++++++++++++++++++++++++
Después de que finalice el proceso de instalación, nos ubicamos en la carpeta que contiene los archivos de configuración de apache. 
Ejecutamos entonces el comando cd /etc/apache2/. Podemos listar el contenido del directorio y vemos los diferentes archivos y subcarpetas.

Nos cercioraremos de que el servicio está corriendo con el comando netstat –tpl. Nos debe aparecer una línea que nos dice que el servicio está
 corriendo y que está escuchando por cualquier dirección IP así:
tcp          0                             *:www                *:*            LISTEN
 
Procedemos entonces a editar los archivos de configuración. El primero será el archivo que especifica los puertos de difusión del servicio. 
Ejecutamos entonces el comando nano ports.conf (ruta relativa)esto en caso de que estemos situados en la carpeta /etc/apache2 de lo contrario 
ejecutamos nano /etc/apache2/ports.conf (ruta absoluta).

Especificaremos entonces la IP de la interface que usaremos reemplazando el * por la dirección correspondiente, en este caso 172.16.0.2.

Especificaremos entonces la IP de la interface que usaremos
Nota: Para guardar los cambios presionamos las teclas Ctrol+X, luego la letra S para afirmar que se guardaran los cambios y luego enter para confirmar 
el nombre del archivo.
Para configurar el sitio global y configuraremos el archivo que ya existe dentro de la carpeta sites-available llamado default.

Nota: Para guardar los cambios presionamos las teclas Ctrol+X, luego la letra S para afirmar que se guardaran los cambios y luego enter para confirmar
 el nombre del archivo.
La configuración predeterminada del archivo es la que aparece a continuación.

Editaremos entonces las siguientes directivas.
DocumentRoot /var/www/=Editamos entonces la ruta de los archivos de la página web. En este caso /var/www/abc como se ve en la siguiente imagen
<Directory /var/www/>= Editamos entonces la ruta de los archivos de la página web. En este caso /var/www/abc como se ve en la siguiente imagen
Order allow,deny=Esta línea la borraremos del archive de configuración.
Allow from all= Esta línea la borraremos del archive de configuración.
Incluiremos además dentro de la etiqueta <Directory /var/www/> la siguiente línea que especifica la página principal del sitio web, en este caso index.html
DirectoryIndex index.html

Nota: Para guardar los cambios presionamos las teclas Ctrol+X, luego la letra S para afirmar que se guardaran los cambios y luego enter para confirmar 
el nombre del archivo.
Cabe aclarar que la carpeta abc de la ruta especificada en el archivo sites-available/default no existe, así como tampoco existe el archivo index.html. 
Por ello los crearemos con los siguientes comandos.
mkdir /var/www/abc Para  crear la carpeta abc en la ruta especificada.
 
Procedemos a editar entonces el archivo index.html con código html sencillo como se ve a continuación. 
Ejecutamos entonces el comando nano /var/www/abc/index.html
 

Nota: Para guardar los cambios presionamos las teclas Ctrol+X, luego la letra S para afirmar que se guardaran los cambios y luego enter para 
confirmar el nombre del archivo.
Procedemos a configurar la interface. Ejecutamos el comando nano /etc/network/interfaces para abrir el archivo de configuración.

Usaremos entonces la interface eth0 y le pondremos la IP especificada en el archivo /etc/apache2/ports.conf, en este caso la 172.16.02/26 como se 
observa a continuación.

Nota: Para guardar los cambios presionamos las teclas Ctrol+X, luego la letra S para afirmar que se guardaran los cambios y luego enter para confirmar 
el nombre del archivo.
Editamos el archivo sites-enabled/000-default. 
Ejecutamos entonces el comando nano sites-enabled/000-default(o con la ruta absoluta nano /etc/apache2/sites-enabled/000-default)

Lo que haremos será reemplazar el * por la IP que hemos acabado de configurar, en este caso 172.16.0.2.

Nota: Para guardar los cambios presionamos las teclas Ctrol+X, luego la letra S para afirmar que se guardaran los cambios y luego enter para 
confirmar el nombre del archivo.
Al terminar la debida configuración debemos reiniciar las interfaces para que adopten la nueva configuración. 
Ejecutamos entonces el comando /etc/init.d/networking restart.

Debemos reiniciar además el servicio para que adopte la nueva configuración. Ejecutamos entonces el comando /etc/init.d/apache2 restart.

Abrimos entonces el explorador para verificar la publicación de el sitio web. Si no se tiene acceso a un servidor dns que haga la traducción del 
nombre de dominio  podemos buscar http://localhost/ o http://172.16.0.2

Podemos ver que el servidor está funcionando debidamente
Si tenemos acceso a un servidor dns que nos haga la traducción del nombre de dominio, podemos buscar http://www.abc.com (nombre de dominio asignado previamente)